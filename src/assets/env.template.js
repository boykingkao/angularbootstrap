(function(window) {
    window["env"] = window["env"] || {};
  
    // Environment variables
    window["env"].ENVIRONMENT_APIURL = "${ENV_APIURL}";
    window["env"].ENVIRONMENT_GOHZA = "${ENV_GOHZA}";

  })(this);