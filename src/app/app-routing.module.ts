import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LicenseManagementComponent } from './license-management/license-management.component';

const routes: Routes = [
  // { path: '', component: AppComponent },
  { path: 'license', component: LicenseManagementComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
