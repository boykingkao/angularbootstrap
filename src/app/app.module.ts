import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'; // Import FormsModule
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';


import { LicenseManagementModule } from './license-management/license-management.module';
import { LicenseManagementComponent } from './license-management/license-management.component';
import { ModalEditLicenseComponent } from './license-management/modal-edit-license/modal-edit-license.component';
import { ModalDeleteLicenseComponent } from './license-management/modal-delete-license/modal-delete-license.component';
import { ModalCreateLicenseComponent } from './license-management/modal-create-license/modal-create-license.component';
import { AgentManagementComponent } from './agent-management/agent-management.component';

@NgModule({
  declarations: [
    AppComponent,
    LicenseManagementComponent,
    ModalEditLicenseComponent,
    ModalDeleteLicenseComponent,
    ModalCreateLicenseComponent,
    AgentManagementComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    LicenseManagementModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
