import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import {
  ModalDismissReasons,
  NgbDatepickerModule,
  NgbModal,
  NgbCalendar,
  NgbDateStruct,
  NgbDate,
} from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { ModalEditLicenseComponent } from './modal-edit-license/modal-edit-license.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-license-management',
  templateUrl: './license-management.component.html',
  styleUrls: ['./license-management.component.scss'],
})
export class LicenseManagementComponent implements OnInit, AfterViewInit {
  @ViewChild('modalEditLicense') modalEditLicense: any;
  @ViewChild('modalDeleteLicense') modalDeleteLicense: any;
  @ViewChild('modalCreateLicense') modalCreateLicense: any;

  ngmodel = '';
  workApi = 'http://localhost:31001/agent/v1/agents/licenses/all-license';
  homeApi = '/assets/license_seat.json';
  ruleApi = '/assets/rules.json';
  tenantApi = '/assets/tenant.json';
  workTenantApi = 'http://192.168.55.40:9786/tenants';

  work: APIs = {
    licenses: 'http://localhost:31001/agent/v1/agents/licenses/all-license',
    tenants: 'http://localhost:31001/agent/v1/agents/tenants',
    rules: 'http://localhost:31001/agent/v1/agents/rules',
    packages: 'http://localhost:31001/agent/v1/agents/packages',
    agents: 'http://localhost:31001/agent/v1/agents/all-agent',
  };
  home: APIs = {
    licenses: '/assets/license_seat.json',
    tenants: '/assets/tenant.json',
    rules: '/assets/rules.json',
    packages: '/assets/packages.json',
    agents: '/assets/agents.json',
  };
  testInput = 'testInput';
  mockupData: data1[] = [];
  mockupDataFilter: data1[];
  ruleList: string[];
  agentData: any[];
  headerData = [];
  rowData: any;
  test: any;
  count: any;
  testText: any = 'gohza';
  tenantData: string[];
  packageData: string[];
  buttonFilter: filterToggle[] = [
    { name: 'Inactive', status: 0, toggle: true },
    { name: 'Active', status: 1, toggle: true },
    { name: 'Expired', status: 2, toggle: true },
  ];
  dateFilters: {
    createDateStart: string;
    createDateEnd: string;
    expireDateStart: string;
    expireDateEnd: string;
  } = {
    createDateStart: '',
    createDateEnd: '',
    expireDateStart: '',
    expireDateEnd: '',
  };

  seatAmount: number;

  licenseDurationList: any[] = [];

  constructor(
    private modalService: NgbModal,
    private http: HttpClient,
    private el: ElementRef
  ) {}

  ngOnInit(): void {
    this.http.get(`${this.work.licenses}`).subscribe((res: any) => {
      console.log(res.responseMessage);
      // this.mockupData = res.responseMessage.sort((a, b) => {
      //   return (
      //     new Date(b.createDate).getTime() - new Date(a.createDate).getTime()
      //   );
      // });;
      // sort by create date ascending
      this.mockupData = res.responseMessage.sort((a, b) => {
        return (
          new Date(a.createDate).getTime() - new Date(b.createDate).getTime()
        );
      });

      this.mockupData.forEach((element) => {
        element.duration = this.duration(element.expDate);
      });
      this.mockupDataFilter = this.mockupData;
      console.log(this.mockupData.filter((item) => item.status === 1).length);
      console.log(this.mockupData.filter((item) => item.status === 0).length);
      console.log(this.mockupData.filter((item) => item.status === 2).length);

      this.licenseDurationList = [
        {
          time: '1 Day',
          amount: this.mockupData.filter((item) => item.duration < 86400)
            .length,
          bgColor: '#FE6D6D',
        },
        {
          time: '7 Days',
          amount: this.mockupData.filter((item) => item.duration < 604800)
            .length,
          bgColor: '#FF834E',
        },
        {
          time: '1 Month',
          amount: this.mockupData.filter((item) => item.duration < 2592000)
            .length,
          bgColor: '#F1F459',
        },
        {
          time: '6 Months',
          amount: this.mockupData.filter((item) => item.duration < 15552000)
            .length,
          bgColor: '#D4FF5A',
        },
        {
          time: '1 Year',
          amount: this.mockupData.filter((item) => item.duration <= 31536000)
            .length,
          bgColor: '#00FF19',
        },
        {
          time: '>1 Year',
          amount: this.mockupData.filter((item) => item.duration > 31536000)
            .length,
          bgColor: '#9C92F8',
        },
      ];
    });

    this.http.get(`${this.work.rules}`).subscribe((res: any) => {
      console.log('rule api');
      this.ruleList = res.responseMessage;
      console.log(res.responseMessage);
    });

    this.http.get(`${this.work.tenants}`).subscribe((res: any) => {
      console.log('tenant Api');
      this.tenantData = res.responseMessage;
      console.log(res);
    });

    this.http.get(`${this.work.packages}`).subscribe((res: any) => {
      console.log('packages Api');
      this.packageData = res.responseMessage;
      console.log(res);
    });

    this.http.get(`${this.work.agents}`).subscribe((res: any) => {
      console.log('agents Api');
      this.agentData = res.responseMessage;
      console.log(res);
    });
  }

  ngAfterViewInit(): void {
    console.log('ngAfterViewInit');
  }

  // countSeat(data:string[]) {
  //   // count object in array
  //   // output:number = data.length;
  //   return data.length;
  // }

  toggleButton(event) {
    console.log(event.target);
    this.isButtonActive = !this.isButtonActive;
  }

  isButtonActive = false;

  toggleButtonColor(buttonColor: HTMLButtonElement) {
    console.log(buttonColor.className);
    this.isButtonActive = !this.isButtonActive;
  }

  onFilterLicense() {
    this.mockupDataFilter = this.mockupData.filter((item: data1) => {
      if (
        (item.status === 0 && !this.buttonFilter[0].toggle) ||
        (item.status === 1 && !this.buttonFilter[1].toggle) ||
        (item.status === 2 && !this.buttonFilter[2].toggle)
      ) {
        return false; // Hide the item
      }

      // Filter based on createDateStart and createDateEnd
      if (
        this.dateFilters.createDateStart !== '' &&
        new Date(item.createDate) < new Date(this.dateFilters.createDateStart)
      ) {
        return false; // Hide the item
      }

      if (
        this.dateFilters.createDateEnd !== '' &&
        new Date(item.createDate) >
          new Date(this.dateFilters.createDateEnd + 'T23:59:59.999Z')
      ) {
        return false; // Hide the item
      }

      // Filter based on expireDateStart and expireDateEnd
      if (
        this.dateFilters.expireDateStart !== '' &&
        new Date(item.expDate) < new Date(this.dateFilters.expireDateStart)
      ) {
        return false; // Hide the item
      }

      if (
        this.dateFilters.expireDateEnd !== '' &&
        new Date(item.expDate) >
          new Date(this.dateFilters.expireDateEnd + 'T23:59:59.999Z')
      ) {
        return false; // Hide the item
      }

      // If none of the conditions match, show the item
      return true;
    });
  }

  onEmitCreateLicense(data) {
    console.log(`data is`);
    // console.log(data);
    const testData: createGroupAndLicenseReq = {
      name: data.name,
      orgId: data.tenant,
      maxSeat: data.maxSeat,
      details: data.rules,
      createBy: 'gohza',
      expDate: data.expDate,
      packageName: data.package,
    };
    console.log(testData);
  }

  onEmitEditLicense(data) {
    console.log(data);
  }

  onEmitDeleteLicense(data) {
    console.log(data);
  }

  onCopy(item: string) {
    console.log(`copy clicked ${item}`);

    // copy to clipboard
    const el = document.createElement('textarea');
    el.value = item;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  }

  onSeeAgentsFilter(item) {
    // alert(`you clicked see agents ${item.ownerId}`);
    let data = {
      active: 3,
      agentUpdateAvailableCount: 0,
      color: '#FF834E',
      expDate: new Date(item.expDate),
      imagOrg: '',
      max: item.maxSeat,
      org: 't',
      orgId: 1,
      package: 't',
      packageId: '2323',
    };
    // let filterPackageId = this.agentData.filter((i) => item.ownerId === i.agentId);

    localStorage.setItem('ownerId', item.ownerId);
    console.log(`${item.ownerId}`);
    console.log(this.agentData);
    console.log(this.agentData.find((i) => item.ownerId === i.packageId));
  }

  onEditLicense(item) {
    // alert('edit clicked');
    console.log(item);

    this.seatAmount = item.seats.length;

    let data = item;

    data.seatAmount = item.seats.length;

    this.modalEditLicense.rowData = data;
    // this.modalEditLicense.seatAmount = item.seats.length;
    this.modalEditLicense.openModal();
    // this.openModal();
  }

  onDeleteLicense(item: any) {
    // open modal-delete-license
    let seatAmount = item.seats.length;
    let ownerId = item.ownerId;
    this.modalDeleteLicense.openModal();
    this.modalDeleteLicense.seatAmount = seatAmount;
    this.modalDeleteLicense.ownerId = ownerId;
  }
  onCreateLicense() {
    // open modal-create-license
    this.modalCreateLicense.openModal();
    // this.modalCreateLicense.resetForm();
  }

  onDelete(item: any) {
    alert('delete clicked');
  }

  duration(expireDate) {
    const ExpireDate = new Date(expireDate);
    const CurrentDate = new Date();

    const durationInMillis = ExpireDate.getTime() - CurrentDate.getTime();
    const durationInSeconds = durationInMillis / 1000;

    return durationInSeconds;
  }

  durationString = (durationInSeconds: number) => {
    const year = Math.floor(durationInSeconds / 31536000);
    const month = Math.floor((durationInSeconds % 31536000) / 2592000);
    const days = Math.floor((durationInSeconds % 2592000) / 86400);
    const hours = Math.floor((durationInSeconds % 86400) / 3600);

    if (year > 0) {
      return `${year}y ${month !== 0 ? month + 'm' : ''}`;
    } else if (month > 0) {
      return `${month}m ${days !== 0 ? days + 'd' : ''}`;
    } else if (days > 0) {
      return `${days}d ${hours !== 0 ? hours + 'd' : ''}h`;
    } else if (hours > 1) {
      return `${hours}h`;
    } else if (durationInSeconds > 0) {
      return `<1 h`;
    } else {
      return `-`;
    }
  };

  createDateFilter(element: HTMLDivElement) {
    // let data = document.querySelectorAll('#createDateFilter');
    // data.forEach((item) => {
    //   (item as HTMLInputElement).style.backgroundColor = '#F1F459';
    // });
    // console.log(data);
  }

  expireDateFilter(elementInput: HTMLDivElement) {
    // let data = document.getElementById('expireDateFilter') as HTMLInputElement;
    // data.style.backgroundColor = 'black';
    // console.log(data);
  }

  toggleDate(createDateStartRef: HTMLInputElement) {}

  closeModal() {
    this.modalService.dismissAll();
  }

  openCalendar(events: HTMLInputElement) {
    const inputTest = document.getElementById(
      'myDateInput'
    ) as HTMLInputElement;

    // open date picker on click

    inputTest.value = 'sadada';
  }
}
interface data1 {
  licId: string;
  serial: string;
  name: string;
  description: string;
  maxSeat: number;
  detail: string;
  createDate: Date;
  updateDate: Date;
  expDate: any;
  createBy: string;
  updateBy: string;
  ownerId: string;
  status: number;
  duration: number;

  //  one to many relations
  seats: string[];
}

interface APIs {
  licenses: string;
  tenants: string;
  rules: string;
  packages?: string;
  agents: string;
}

interface createGroupAndLicenseReq {
  name: string;
  orgId: string;
  maxSeat: number;
  details: details[];
  createBy: string;
  expDate: Date;
  packageName: string;
}

interface details {
  ip: string;
  port: number;
  ruleId: string;
}

interface filterToggle {
  name: string;
  status: number;
  toggle: boolean;
}

// license Status
// NotUse = 0,
// Use = 1,
// Expire = 2

interface pieChart {
  color: string[];
  tooltip: {
    trigger: string;
  };
  series: {
    type: string;
    radius: string;
    label: {
      color: string;
      position: string;
    };
    data: {
      color: string;
      name: string;
      value: number;
    }[];
  }[];
}
