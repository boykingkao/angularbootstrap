import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { ModalEditLicenseComponent } from './modal-edit-license/modal-edit-license.component';
import { ModalDeleteLicenseComponent } from './modal-delete-license/modal-delete-license.component';
import { ModalCreateLicenseComponent } from './modal-create-license/modal-create-license.component';

@NgModule({
  declarations: [
    
  ],
  imports: [CommonModule, NgbModule],
  exports: [
   
  ],
})
export class LicenseManagementModule {}
