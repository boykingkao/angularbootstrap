import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { ModalCreateLicenseService } from './modal-create-license.service';

@Component({
  selector: 'app-modal-create-license',
  templateUrl: './modal-create-license.component.html',
  styleUrls: ['./modal-create-license.component.scss'],
})
export class ModalCreateLicenseComponent implements OnInit {
  @ViewChild('modalCreate') modalCreate: any;
  @ViewChild('gohZa') gohZa: any;
  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private ModalCreateLicenseService: ModalCreateLicenseService
  ) {}

  @Input() ruleList: string[];
  @Input() tenantData: string[];
  @Input() packageData: string[];
  @Output() confirmCreate = new EventEmitter<string>();
  isChecked: boolean = true; // Set the initial state
  testName: string;
  createText: string = 'modalCreatePage';
  createForm: FormGroup;
  testDiv = '';

  ngOnInit(): void {
    this.createForm = this.fb.group({
      name: [
        '',
        [
          this.ModalCreateLicenseService.Validation.minLength(5),
          this.ModalCreateLicenseService.Validation.maxLength(32),
        ],
      ],
      tenant: ['', [this.ModalCreateLicenseService.Validation.required,]],
      package: ['', [this.ModalCreateLicenseService.Validation.required,]],
      maxSeat: [
        '',
        [
          this.ModalCreateLicenseService.Validation.required,
          this.ModalCreateLicenseService.Validation.pattern(/^[1-9][0-9]*$/),
        ],
      ],
      expDate: [
        '',
        [
          this.ModalCreateLicenseService.Validation.required,
          this.ModalCreateLicenseService.Validation.dateNotLessThanCurrentDate,
        ],
      ],
      description: ['', [this.ModalCreateLicenseService.Validation.maxLength(255)]],
      rules: this.fb.array([
        // this.fb.group({
        //   isChecked: [false],
        //   rule: [''],
        //   ip: [''],
        //   port: [''],
        // }),
      ]),
    });
  }

  resetForm() {
    this.createForm.reset();
    // while (this.ruleForm.length > 0) {
    //   this.ruleForm.removeAt(0);
    // }
    this.ruleForm.clear();
  }

  get ruleForm() {
    return this.createForm.get('rules') as FormArray;
  }

  ruleIndex(i, controlName) {
    return this.ruleForm.controls[i].get(`${controlName}`);
  }

  onAddRule() {
    const control = new FormGroup({
      isChecked: new FormControl(false),
      rule: new FormControl(''),
      ip: new FormControl(''),
      port: new FormControl(''),
    });
  }

  onDeleteRule() {}

  onSubmit(modal) {

    Object.keys(this.createForm.controls).map((control) => {
  
      this.createForm.get(control).markAsDirty();
    })

    this.ruleForm.controls.map((control,i) => {
      if(control.value.isChecked === true){
        control.get('ip').markAsDirty();
        control.get('port').markAsDirty();
      }else{
        control.get('ip').markAsPristine();
        control.get('port').markAsPristine();
      }
    });
    
    
    const rules_checkValid: boolean[] = (
      this.createForm.controls.rules as FormArray
    ).controls
      .filter((item) => item.value.isChecked === true)
      .map((item) => item.valid);
    const allRulesAreTrue = rules_checkValid.every((value) => value === true);

    console.log(rules_checkValid);
    if (
      this.createForm.controls.name.valid &&
      this.createForm.controls.tenant.valid &&
      this.createForm.controls.package.valid &&
      this.createForm.controls.maxSeat.valid &&
      this.createForm.controls.expDate.valid &&
      this.createForm.controls.description.valid &&
      rules_checkValid.length > 0 &&
      allRulesAreTrue
    ) {
      const selectedRule = this.createForm.value.rules
        .filter((item) => item.isChecked === true)
        .map((obj) => {
          const { isChecked, ruleId, rule, ip, port } = obj;
          return { ip, port, ruleId };
        });
      let newData = this.createForm.value;
      // change expire date hour time to 23:59:59
      newData.expDate = new Date(newData.expDate + 'T23:59:59.999Z');
      newData.rules = selectedRule;

      this.confirmCreate.emit(newData);
      // modal.close();
    } else {
      alert('Some inputs are invalid, required at least 1 rule');
    }
  }

  openModal() {
    this.modalService.open(this.modalCreate, { size: 'xl', centered: true });
    this.resetForm();
    // push tenant data to rule form
    this.ruleList.forEach((element: any) => {
      const control = new FormGroup({
        isChecked: new FormControl(false),
        ruleId: new FormControl(`${element.ruleId}`),
        rule: new FormControl(`${element.name}`),
        ip: new FormControl("", [
          this.ModalCreateLicenseService.Validation.minLength(5),
          this.ModalCreateLicenseService.Validation.maxLength(32),

        ]),
        port: new FormControl("", [
          this.ModalCreateLicenseService.Validation.required,
          this.ModalCreateLicenseService.Validation.portValidator,
        ]),
      });
      this.ruleForm.push(control);
  
    });
  }
}
