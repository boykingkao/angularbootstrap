import { TestBed } from '@angular/core/testing';

import { ModalCreateLicenseService } from './modal-create-license.service';

describe('ModalCreateLicenseService', () => {
  let service: ModalCreateLicenseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalCreateLicenseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
