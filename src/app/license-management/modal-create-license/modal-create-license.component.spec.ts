import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreateLicenseComponent } from './modal-create-license.component';

describe('ModalCreateLicenseComponent', () => {
  let component: ModalCreateLicenseComponent;
  let fixture: ComponentFixture<ModalCreateLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCreateLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
