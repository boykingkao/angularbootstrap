import {
  Component,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  OnInit,
  AfterViewChecked,
  AfterContentInit,
  OnChanges,
  AfterViewInit,
  SimpleChanges,
} from '@angular/core';
import {
  ModalDismissReasons,
  NgbDatepickerModule,
  NgbModal,
} from '@ng-bootstrap/ng-bootstrap';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { ModalEditLicenseService } from './modal-edit-license.service';

@Component({
  selector: 'app-modal-edit-license',
  templateUrl: './modal-edit-license.component.html',
  styleUrls: ['./modal-edit-license.component.scss'],
})
export class ModalEditLicenseComponent
  implements OnInit, OnChanges, AfterViewChecked
{
  @ViewChild('insideModal') insideModal: any;

  @Input() rowData: any;
  @Input() seatAmount: any;
  @Output() saveEdit = new EventEmitter<string>();
  dateData: any;
  formatDate: any = '';
  testDate: any = '';
  testText: any = 'gohza';
  createText: string = 'modalCreatePage';
  editForm: FormGroup;

  defaultExpDate: Date;


  constructor(
    private modalService: NgbModal,
    private ModalEditLicenseService: ModalEditLicenseService,
    private fb: FormBuilder
  ) {
    this.editForm = this.fb.group({
      licId: [null],
      serial: [null],
      name: [
        null,
        [
          this.ModalEditLicenseService.Validation.minLength(5),
          this.ModalEditLicenseService.Validation.maxLength(32),
        ],
      ],
      description: [
        null,
        [this.ModalEditLicenseService.Validation.maxLength(255)],
      ],
      maxSeat: [
        null,
        [
          // Validators.required,
          // Validators.pattern(/^[1-9][0-9]*$/),
          // this.ModalEditLicenseService.maxSeatIsMorethanSeatAmount(
          //   this.seatAmount
          // ),
        ],
      ],
      detail: [null],
      createDate: [null],
      updateDate: [null],
      expDate: [
        null,
        [
          Validators.required,
          this.ModalEditLicenseService.Validation.dateNotLessThanCurrentDate,
        ],
      ],
      createBy: [null],
      updateBy: [null],
      ownerId: [null],
      status: [null],
      duration: [null],
    });
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {}

  ngAfterViewChecked(): void {
    // add another validation to a control of editForm

    this.editForm
      .get('maxSeat')
      .setValidators([
        this.ModalEditLicenseService.Validation.pattern(/^[1-9][0-9]*$/),
        this.ModalEditLicenseService.Validation.maxSeatIsMorethanSeatAmount(
          this.seatAmount
        ),
      ]);

    // console.log(this.editForm.controls.maxSeat);
    // if control got error
    // console.log(this.editForm.controls.maxSeat.errors);
  }

  getDate() {
    return new Date();
  }

  testEmit() {
    this.saveEdit.emit(this.rowData);
  }

  onSubmit(modal) {
    Object.keys(this.editForm.controls).map((control) => {
      // this.createForm.controls[key].markAsDirty();

      this.editForm.get(control).markAsDirty();
    });

    // console.log('form submitted');
    let output = this.editForm.value;
    console.log(`output.expDate date : ${output.expDate}`);
    console.log(`default expire date : ${this.defaultExpDate}`);

    let isExpDateSame:boolean =
      new Date(this.defaultExpDate).getTime() ==
      new Date(output.expDate).getTime();

    

    output.status = parseInt(output.status);

    console.log(`is date same : ${isExpDateSame}`);
    console.log(this.editForm);
    if (this.editForm.valid) {
      output.expDate = new Date(output.expDate + 'T23:59:59.999Z');
      this.saveEdit.emit(output);
      modal.close();
      console.log('ok all valid')
    } else if (
      this.editForm.controls.name.valid &&
      this.editForm.controls.description.valid &&
      this.editForm.controls.maxSeat.valid &&
      !this.editForm.controls.expDate.valid &&
      isExpDateSame
    ) {
      // output.expDate = new Date(output.expDate + 'T23:59:59.999Z');
      output.expDate = new Date(this.rowData.expDate);


      console.log('date same no need to check')
      this.saveEdit.emit(output);
      modal.close();
    } else {
      alert('Some inputs are invalid');
    }
  }

  onDateSelect(event) {
    console.log(event);
    // convert data to 2023-01-24T09:35:42.865Z as example
    this.dateData = new Date(event.year, event.month - 1, event.day);
    this.rowData.expDate = this.dateData;
    // this.editForm.patchValue({
    //   expDate: this.dateData,
    // });
    console.log(this.dateData);
  }

  onCopy(item: string) {
    console.log(`copy clicked ${item}`);

    // copy to clipboard
    const el = document.createElement('textarea');
    el.value = item;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  }

  onSelectStatus(event: HTMLSelectElement) {
    // click event open dropdown

    console.log(event.selectedOptions[0].value);
    this.editForm.patchValue({
      status: parseInt(event.selectedOptions[0].value),
    });
  }

  closeModal() {
    this.modalService.dismissAll();
    // this.modalService.open(this.content)
  }

  openModal() {
    // this.editForm.reset();
    this.modalService.open(this.insideModal, { size: 'xl' });

    this.editForm.markAsPristine();

    console.log('max seat = ' + this.rowData.maxSeat);
    console.log('current seat = ' + this.seatAmount);

    this.defaultExpDate = this.rowData.expDate.slice(0, this.rowData.expDate.indexOf('T'));

    this.editForm.patchValue({
      licId: this.rowData.licId,
      serial: this.rowData.serial,
      name: this.rowData.name,
      description: this.rowData.description,
      maxSeat: this.rowData.maxSeat,
      detail: this.rowData.detail,
      createDate: this.rowData.createDate,
      updateDate: this.rowData.updateDate,
      expDate: this.rowData.expDate.slice(0, this.rowData.expDate.indexOf('T')),
      createBy: this.rowData.createBy,
      updateBy: this.rowData.updateBy,
      ownerId: this.rowData.ownerId,
      status: this.rowData.status,
      duration: this.rowData.duration,
    });
    console.log(this.editForm.value);
  }

  dateCheck() {
    let dateNow = new Date();
    let formDate = new Date(this.editForm.value.expDate);
    return formDate < dateNow;
  }
}

interface licenseStatus {
  name: string;
  value: number;
}
