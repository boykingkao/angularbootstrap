import { Injectable } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ModalEditLicenseService {
  constructor() {}

  Validation = new Validation();
}

class Validation {
  constructor() {}

  required(control: AbstractControl): ValidationErrors | null {
    if (control.value == (null || undefined )) {
      return { required: 'required text' };
    }

    if (typeof control.value === 'string') {
      if (control.value.split("").length == 0) {
        return { required: 'required text' };
      }
    }

    return null;

  
  }

  minLength(length: number): ValidatorFn {

    return (control: AbstractControl): ValidationErrors | null => {
      // let parentControl = control.parent;
      
      if (control.value != null && control.value.length < length || this.required(control) !== null) {
        return { minLength: `need at least ${length} letters` };
      }
      return null;
    };
  }

  maxLength(length: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let parentControl = control.parent;
      if (control.value != null && control.value.length > length ) {
        return { maxLength: `can't exceed ${length} letters` };
      }
      return null;
    };
  }

  dateNotLessThanCurrentDate(control: any): { [key: string]: string } | null {
    let selectedDate: Date;
    if (control.value != null) {
      selectedDate = new Date(control.value + 'T23:59:59.999Z');
    }
    const currentDate = new Date();

    if (selectedDate < currentDate) {
      return {
        dateNotLessThanCurrentDate: "date can't be less than current date",
      };
    }

    return null;
  }

  maxSeatIsMorethanSeatAmount(seatAmount: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      // seat amount คือ จำนวน agent ที่ใช้งาน license ในปัจจุบัน
      const maxSeat = control.value;
      if (maxSeat < seatAmount && control.value) {
        return {
          maxSeatIsMorethanSeatAmount: 'Max seat is less than current seats',
        };
      }

      return null; // Validation successful
    };
  }

  pattern(pattern: RegExp): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!pattern.test(control.value) && control.value) {
        return { pattern: "invalid pattern, must start with '1-9' " };
      }
      return null;
    };
  }

  portValidator = (control: FormControl): { [s: string]: any } => {

    // if (this.required(control) != null){
    //   return this.required(control);
    // }

    if (isNaN(control.value)) {
      return { portValidator: "port must be number" };
    }
    const port = control.value;

    if (port < 0 || port > 65535) {
      return { portValidator: "port must be between 0 and 65535" };
    }
    return null;
  };
}