import { TestBed } from '@angular/core/testing';

import { ModalEditLicenseService } from './modal-edit-license.service';

describe('ModalEditLicenseService', () => {
  let service: ModalEditLicenseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalEditLicenseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
