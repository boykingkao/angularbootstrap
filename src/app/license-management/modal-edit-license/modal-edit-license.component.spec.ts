import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditLicenseComponent } from './modal-edit-license.component';

describe('ModalEditLicenseComponent', () => {
  let component: ModalEditLicenseComponent;
  let fixture: ComponentFixture<ModalEditLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalEditLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
