import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-delete-license',
  templateUrl: './modal-delete-license.component.html',
  styleUrls: ['./modal-delete-license.component.scss'],
})
export class ModalDeleteLicenseComponent implements OnInit {
  @ViewChild('modalDelete') modalDelete: any;
  constructor(private modalService: NgbModal) {}

  @Output() confirmDelete = new EventEmitter<string>();
  createText: string = 'modalCreatePage';
  seatAmount:number = 0;
  ownerId:string = "";

  ngOnInit(): void {}

  onDelete(){
    this.confirmDelete.emit(this.ownerId);
  }
  openModal() {
    this.modalService.open(this.modalDelete, { size: 'sm', centered: true });
  }
}
