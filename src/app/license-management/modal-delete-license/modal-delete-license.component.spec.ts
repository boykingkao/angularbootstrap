import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteLicenseComponent } from './modal-delete-license.component';

describe('ModalDeleteLicenseComponent', () => {
  let component: ModalDeleteLicenseComponent;
  let fixture: ComponentFixture<ModalDeleteLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDeleteLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDeleteLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
