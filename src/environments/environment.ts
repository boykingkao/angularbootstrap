// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import * as process from "process";

const data = process.env.ENVIRONMENT_LOCALHOST

export const environment = {
  production: false,
  text1: "sumet",
  text2: "kongkaew",
  // localhost: "http://localhost:4200",
  localhost: data || 'empty variable',
  apiurl: window["env"].ENVIRONMENT_APIURL || "empty apiurl",
  gohza: window["env"].ENVIRONMENT_GOHZA || "empty gohza",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
